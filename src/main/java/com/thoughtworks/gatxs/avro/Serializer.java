package com.thoughtworks.gatxs.avro;

import java.io.File;
import java.io.IOException;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;

class Serializer {

    public static void main(String[] args) throws IOException {
        Serializer obj = new Serializer();
        ClassLoader loader = obj.getClass().getClassLoader();
        File schemaFile = new File(loader.getResource("emp.avsc").getFile());
        Schema schema = new Schema.Parser().parse(schemaFile);

        GenericRecord e1 =new GenericData.Record(schema);
        e1.put("name", "Bob");
        e1.put("id", 001);
        e1.put("salary", 30000);
        e1.put("age", 25);
        e1.put("address", "Quito");

        GenericRecord e2 =new GenericData.Record(schema);
        e2.put("name", "Alice");
        e2.put("id", 002);
        e2.put("salary", 60000);
        e2.put("age", 32);
        e2.put("address", "Quito");

        DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(
            schema);
        DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(
                datumWriter);
        File outFile = new File(schemaFile.getParent() + "/output.txt");
        System.out.println(">>>>>>>>>>" + outFile.getAbsolutePath());
        dataFileWriter.create(schema, outFile);
        dataFileWriter.append(e1);
        dataFileWriter.append(e2);
        dataFileWriter.close();
        System.out.println("data successfully serialized");
    }

}