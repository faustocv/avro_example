override GRADLE_IMAGE := gradle:5.2.1-jdk11-slim
ARG = gradle -version

gd:
	docker container run \
	  --rm \
	  -it \
	  -v $(shell pwd):/app \
	  --name gradle_container \
	  -w /app \
	  $(GRADLE_IMAGE) \
	  $(ARG)

run:
	docker container run \
	  --rm \
	  -it \
	  -v $(shell pwd):/app \
	  --name gradle_container \
	  -w /app \
	  $(GRADLE_IMAGE) \
	  gradle -PmainClass=com.thoughtworks.gatxs.avro.Serializer execute
